import { Component, OnInit } from "@angular/core";
import { Task } from "../models/task.model";
import { NotificationService } from "../service/notification.service";
import { TaskService } from "../service/task.service";
import { taskDataService } from "../service/datastorage.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-add-task",
  templateUrl: "./add-task.component.html",
  styleUrls: ["./add-task.component.scss"]
})
export class AddTaskComponent implements OnInit {
  task: Task = new Task();
  isSubmitDisabled = false;
  isUpdateDisabled = true;
  title = "Add";
  constructor(
    private router: Router,
    private notifyService: NotificationService,
    private taskService: TaskService,
    private dataStore: taskDataService
  ) {
    this.task = this.dataStore.getTaskData();
    if (this.task.id != null) {
      console.log(this.dataStore.getTaskData());
      this.isSubmitDisabled = true;
      this.isUpdateDisabled = false;
      this.title = "Update";
    } else {
      this.task = new Task();
      this.dataStore.setTaskData(this.task);
      this.isSubmitDisabled = false;
      this.isUpdateDisabled = true;
      this.title = "Add";
    }
  }

  ngOnInit() {}
  showToaster(message) {
    this.notifyService.showSuccess(message, "Task Info");
  }
  showToasterWithTimeout() {
    this.notifyService.showSuccessWithTimeout(
      "Data shown successfully !!",
      "Notification",
      1500
    );
  }

  createTask(): void {
    this.taskService.createTask(this.task).subscribe(data => {
      console.log(this.task);
      this.showToaster(this.task.task + " created successfully");
      this.ngOnInit();
      this.task = new Task();
    });
  }

  updateTask(): void {
    this.taskService.updateTask(this.task).subscribe(data => {
      this.showToaster(this.task.task + " updated successfully");
      this.task = new Task();
      this.dataStore.setTaskData(this.task);
      this.router.navigate(["list"]);
    });
  }
}
