import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import { MdbTableDirective } from "angular-bootstrap-md";
import { Router } from "@angular/router";
import { taskDataService } from "../service/datastorage.service";
import { Task } from "../models/task.model";
import { TaskService } from "../service/task.service";
import { NotificationService } from "../service/notification.service";
@Component({
  selector: "app-list-task",
  templateUrl: "./list-task.component.html",
  styleUrls: ["./list-task.component.scss"]
})
export class ListTaskComponent implements OnInit {
  tasks: Task[] = [];
  task: Task;
  length = 0;

  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  elements: any = [];
  headElements = ["task", "parentTask", "priority", "startDate", "endDate"];

  searchText: string = "";
  previous: string;
  isDisabled: true;

  constructor(
    private router: Router,
    private taskService: TaskService,
    private dataStore: taskDataService,
    private notifyService: NotificationService
  ) {
    this.task = new Task();
    this.dataStore.setTaskData(this.task);
  }

  @HostListener("input") oninput() {
    this.searchItems();
  }

  ngOnInit() {
    this.taskService.getTask().subscribe(data => {
      data.forEach(entry => {
        this.elements.push(entry);
      });
    });

    this.mdbTable.setDataSource(this.elements);
    this.elements = this.mdbTable.getDataSource();
    this.previous = this.mdbTable.getDataSource();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }
  showToaster(message) {
    this.notifyService.showWarning(message, "Task Info");
  }
  updateTask(task: Task): void {
    console.log("update");
    console.log(task);
    this.dataStore.setTaskData(task);
    this.router.navigate(["add"]);
  }

  endTask(task: Task): void {
    console.log("end");
    task.enabled = true;
    console.log(task);
    this.taskService.updateTask(task).subscribe(data => {
      this.showToaster(task.task + " has been ended");
    });
  }
}
