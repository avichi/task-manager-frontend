import { Component } from "@angular/core";
import { taskDataService } from "./service/datastorage.service";
import { Task } from "./models/task.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  task: Task;
  title = "Task Manager App";
  constructor(private dataStore: taskDataService) {
    this.task = new Task();
    this.dataStore.setTaskData(this.task);
  }
}
