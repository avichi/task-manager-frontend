import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AddTaskComponent } from "./add-task/add-task.component";
import { ListTaskComponent } from "./list-task/list-task.component";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing.module";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { FormsModule } from "@angular/forms";
import { ToastrModule } from "ngx-toastr";
import { NotificationService } from "./service/notification.service";
import { TaskService } from "./service/task.service";
import { HttpClientModule } from "@angular/common/http";
import { taskDataService } from "./service/datastorage.service";

@NgModule({
  declarations: [AppComponent, AddTaskComponent, ListTaskComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [NotificationService, TaskService, taskDataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
