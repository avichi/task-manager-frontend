import { Injectable } from "@angular/core";
import { Task } from "../models/task.model";

@Injectable()
export class taskDataService {
  taskData: Task;
  getTaskData() {
    return this.taskData;
  }
  setTaskData(data: Task) {
    this.taskData = data;
  }
}
