import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Task } from "../models/task.model";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class TaskService {
  constructor(private http: HttpClient) {}

  private taskUrl = "http://localhost:8090/task-portal/tasks";

  //private userUrl = '/api';

  public getTask() {
    return this.http.get<Task[]>(this.taskUrl);
  }

  public createTask(task) {
    return this.http.post<Task>(this.taskUrl, task);
  }

  public updateTask(task) {
    return this.http.put<Task>(this.taskUrl + "/" + task.id, task);
  }
}
