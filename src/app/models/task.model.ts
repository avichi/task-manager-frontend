export class Task {
  id: number;
  task: string;
  parentTask: string;
  startDate: Date;
  endDate: Date;
  priority: number;
  enabled: boolean;
}
