import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddTaskComponent } from "./add-task/add-task.component";
import { ListTaskComponent } from "./list-task/list-task.component";

const routes: Routes = [
  { path: "list", component: ListTaskComponent },
  { path: "add", component: AddTaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
